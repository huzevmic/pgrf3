#version 150

float getShadowFactor();

in vec3 normal;
in vec3 light;
in vec3 viewDirection;
in vec4 depthTextureCoord;
in vec3 vertColorXYZ;
in vec2 texCoord;

uniform sampler2D depthTexture;
uniform sampler2D textureBricks;
uniform int colorMode;
uniform int type;

const float delta = 1e-4;

out vec4 outColor; // output from the fragment shader

void main() {
    vec4 ambient = vec4(vec3(0.20), 1);

    float NdotL = max(0, dot(normalize(normal), normalize(light)));
    vec4 diffuse = vec4(NdotL * vec3(0.35), 1);

    vec3 halfVector = normalize(normalize(light) + normalize(viewDirection));
    float NdotH = dot(normalize(normal), halfVector);
    vec4 specular = vec4(pow(NdotH, 16) * vec3(0.45), 1);

    vec4 textureColor = texture(textureBricks, texCoord);

    float shadowFactor = getShadowFactor();

    switch (colorMode) {
        case 0:
        if ( type == 6 ) {
            outColor = vec4(1,1,0,1);
        } else {
            outColor = (ambient + (diffuse + specular) * shadowFactor) * textureColor;
        }
        break;
        case 1:
        outColor = vec4(vertColorXYZ, 1);
        break;
        case 2:
        outColor = ambient + (diffuse + specular) * shadowFactor;
        break;
        case 3:
        outColor = vec4(normalize(normal), 1);
        break;
        case 4:
        outColor = vec4(texCoord, 1, 1);
        break;
    }
}


// hladsi prechody u stinu
// shadowFactor: 0 - ve stinu, 1 - na svetle
float getShadowFactor() {
    float shadowFactor = 0;
    vec2 inc = 1.0 / textureSize(depthTexture, 0);
    for(int row = -1; row <= 1; ++row)
    {
        for(int col = -1; col <= 1; ++col)
        {
            float textDepth = texture(depthTexture, depthTextureCoord.xy + vec2(row, col) * inc).r;
            shadowFactor += depthTextureCoord.z - delta > textDepth ? 1.0 : 0.0;
        }
    }
    shadowFactor /= 9.0;
    shadowFactor = 1 - shadowFactor;
    return shadowFactor;
}
