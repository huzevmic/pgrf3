#version 150
in vec2 inPosition; // input from the vertex buffer

uniform mat4 view;
uniform mat4 projection;
uniform float time;
uniform float camShift;
uniform int type;

const float PI = 3.141592;


vec3 getCartesian1(vec2 vec) {
    float x = vec.x * 1.2;
    float y = vec.y;
    float z = -0.5 * (sin(vec.x*PI+time*2)*sin(vec.x*PI+time*2)+sin(vec.x*PI+time));
    return vec3(x + 4.5, y + 2, z);
}

vec3 getCartesian2(vec2 vec) {
    float s = vec.x * PI;
    float t = vec.y / 2;

    float size = 0.5;
    float x = ( 2*cos(s)+t*cos(s/2) ) * size;
    float y = ( 2*sin(s)+t*cos(s/2) ) * size;
    float z = t*sin(s/2) * 3 * size;
    return vec3(x, y - 2, z);
}

vec3 getSpherical1(vec2 vec) {
    float az = vec.x * PI;
    float ze = vec.y * PI / 2;
    float r = 0.8;

    float x = r * cos(az) * cos(ze) + sin(time) / 2;
    float y = r * sin(az) * cos(ze) + sin(time) / 2;
    float z = r * sin(ze) * 0.6 + cos(az) * 0.5;
    return vec3(x - 4.5, y + 2, z);
}

vec3 getSpherical2(vec2 vec) {
    float az = vec.x * PI;
    float ze = vec.y * PI / 2;
    float r = 3 + cos(4 * az);

    float size = 0.3;
    float x = r * cos(az) * cos(ze) * size;
    float y = r * sin(az) * cos(ze) * size;
    float z = r * sin(ze) * size;
    return vec3(x - 4.5, y - 2, z);
}

vec3 getCylindrical1(vec2 vec) {
    float r = vec.y * PI;
    float theta = vec.x * PI;
    float z = r;

    float size = 0.4;
    float x = r * cos(theta) * size;
    float y = r * sin(theta) * size;
    z = z * size;
    return vec3(x + 4.5, y - 2, z);
}

vec3 getCylindrical2(vec2 vec) {
    float s = ( vec.x + 1 ) * PI;
    float t = ( vec.y * 0.875 - 0.125 ) * PI;

    float r = 1 + sin(t);
    float theta = s;
    float z = t;

    float size = 0.4;
    float x = r * cos(theta) * size;
    float y = r * sin(theta) * size;
    z = z * size;
    return vec3(x, y + 2, z);
}

vec3 getLightSphere(vec2 vec) {
    float az = vec.x * PI;
    float ze = vec.y * PI / 2;
    float r = 1;

    float size = 0.6;
    float x = r * cos(az) * cos(ze) * size;
    float y = r * sin(az) * cos(ze) * size;
    float z = r * sin(ze) * size;
    return vec3(x + 6, y + 3, z + camShift);
}

vec3 getPlane(vec2 vec){
    float x = vec.x * 9;
    float y = vec.y * 6;
    return vec3(x, y, -2);
}


void main() {
    vec2 position = inPosition * 2 - 1;
    vec4 pos4;

    switch (type) {
        case 0:
        pos4 = vec4(getCartesian1(position), 1.0);
        break;
        case 1:
        pos4 = vec4(getCartesian2(position), 1.0);
        break;
        case 2:
        pos4 = vec4(getSpherical1(position), 1.0);
        break;
        case 3:
        pos4 = vec4(getSpherical2(position), 1.0);
        break;
        case 4:
        pos4 = vec4(getCylindrical1(position), 1.0);
        break;
        case 5:
        pos4 = vec4(getCylindrical2(position), 1.0);
        break;
        case 6:
        pos4 = vec4(getLightSphere(position), 1.0);
        break;
        case 7:
        pos4 = vec4(getPlane(position), 1.0);
        break;
    }

	gl_Position = projection * view * pos4;
}
