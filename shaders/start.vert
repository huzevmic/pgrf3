#version 150
in vec2 inPosition; // input from the vertex buffer

uniform mat4 view;
uniform mat4 projection;
uniform float time;
uniform mat4 lightViewProjection;
uniform float camShift;
uniform int type;

out vec3 vertColorXYZ;
out vec3 normal;
out vec3 light;
out vec3 viewDirection;
out vec4 depthTextureCoord;
out vec2 texCoord;

const float PI = 3.141592;
const float delta = 1e-4;


vec3 getCartesian1(vec2 vec) {
    float x = vec.x * 1.2;
    float y = vec.y;
    float z = -0.5 * (sin(vec.x*PI+time*2)*sin(vec.x*PI+time*2)+sin(vec.x*PI+time));
    return vec3(x + 4.5, y + 2, z);
}

vec3 getCartesian2(vec2 vec) {
    float s = vec.x * PI;
    float t = vec.y / 2;

    float size = 0.5;
    float x = ( 2*cos(s)+t*cos(s/2) ) * size;
    float y = ( 2*sin(s)+t*cos(s/2) ) * size;
    float z = t*sin(s/2) * 3 * size;
    return vec3(x, y - 2, z);
}

vec3 getSpherical1(vec2 vec) {
    float az = vec.x * PI;
    float ze = vec.y * PI / 2;
    float r = 0.8;

    float x = r * cos(az) * cos(ze) + sin(time) / 2;
    float y = r * sin(az) * cos(ze) + sin(time) / 2;
    float z = r * sin(ze) * 0.6 + cos(az) * 0.5;
    return vec3(x - 4.5, y + 2, z);
}

vec3 getSpherical2(vec2 vec) {
    float az = vec.x * PI;
    float ze = vec.y * PI / 2;
    float r = 3 + cos(4 * az);

    float size = 0.3;
    float x = r * cos(az) * cos(ze) * size;
    float y = r * sin(az) * cos(ze) * size;
    float z = r * sin(ze) * size;
    return vec3(x - 4.5, y - 2, z);
}

vec3 getCylindrical1(vec2 vec) {
    float r = vec.y * PI;
    float theta = vec.x * PI;
    float z = r;

    float size = 0.4;
    float x = r * cos(theta) * size;
    float y = r * sin(theta) * size;
    z = z * size;
    return vec3(x + 4.5, y - 2, z);
}

vec3 getCylindrical2(vec2 vec) {
    float s = ( vec.x + 1 ) * PI;
    float t = ( vec.y * 0.875 - 0.125 ) * PI;

    float r = 1 + sin(t);
    float theta = s;
    float z = t;

    float size = 0.4;
    float x = r * cos(theta) * size;
    float y = r * sin(theta) * size;
    z = z * size;
    return vec3(x, y + 2, z);
}

vec3 getLightSphere(vec2 vec) {
    float az = vec.x * PI;
    float ze = vec.y * PI / 2;
    float r = 1;

    float size = 0.6;
    float x = r * cos(az) * cos(ze) * size;
    float y = r * sin(az) * cos(ze) * size;
    float z = r * sin(ze) * size;
    return vec3(x + 6, y + 3, z + camShift);
}

vec3 getPlane(vec2 vec){
    float x = vec.x * 9;
    float y = vec.y * 6;
    return vec3(x, y, -2);
}


vec3 getCartesian1Normal(vec2 vec) {
    vec3 u = getCartesian1(vec + vec2(delta, 0)) - getCartesian1(vec - vec2(delta, 0));
    vec3 v = getCartesian1(vec + vec2(0, delta)) - getCartesian1(vec - vec2(0, delta));
    return cross(u, v);
}
vec3 getCartesian2Normal(vec2 vec) {
    vec3 u = getCartesian2(vec + vec2(delta, 0)) - getCartesian2(vec - vec2(delta, 0));
    vec3 v = getCartesian2(vec + vec2(0, delta)) - getCartesian2(vec - vec2(0, delta));
    return cross(u, v);
}
vec3 getSpherical1Normal(vec2 vec) {
    vec3 u = getSpherical1(vec + vec2(delta, 0)) - getSpherical1(vec - vec2(delta, 0));
    vec3 v = getSpherical1(vec + vec2(0, delta)) - getSpherical1(vec - vec2(0, delta));
    return cross(u,v);
}
vec3 getSpherical2Normal(vec2 vec) {
    vec3 u = getSpherical2(vec + vec2(delta, 0)) - getSpherical2(vec - vec2(delta, 0));
    vec3 v = getSpherical2(vec + vec2(0, delta)) - getSpherical2(vec - vec2(0, delta));
    return cross(u,v);
}
vec3 getCylindrical1Normal(vec2 vec) {
    vec3 u = getCylindrical1(vec + vec2(delta, 0)) - getCylindrical1(vec - vec2(delta, 0));
    vec3 v = getCylindrical1(vec + vec2(0, delta)) - getCylindrical1(vec - vec2(0, delta));
    return cross(u, v);
}
vec3 getCylindrical2Normal(vec2 vec) {
    vec3 u = getCylindrical2(vec + vec2(delta, 0)) - getCylindrical2(vec - vec2(delta, 0));
    vec3 v = getCylindrical2(vec + vec2(0, delta)) - getCylindrical2(vec - vec2(0, delta));
    return cross(u, v);
}
vec3 getLightSphereNormal(vec2 vec) {
    vec3 u = getLightSphere(vec + vec2(delta, 0)) - getLightSphere(vec - vec2(delta, 0));
    vec3 v = getLightSphere(vec + vec2(0, delta)) - getLightSphere(vec - vec2(0, delta));
    return cross(u,v);
}
vec3 getPlaneNormal(vec2 vec) {
    vec3 u = getPlane(vec + vec2(delta, 0)) - getPlane(vec - vec2(delta, 0));
    vec3 v = getPlane(vec + vec2(0, delta)) - getPlane(vec - vec2(0, delta));
    return cross(u,v);
}


void main() {
    vec2 position = inPosition * 2 - 1;
    vec4 pos4;

    switch (type) {
        case 0:
        pos4 = vec4(getCartesian1(position), 1.0);
        normal = mat3(view) * getCartesian1Normal(position);
        break;
        case 1:
        pos4 = vec4(getCartesian2(position), 1.0);
        normal = mat3(view) * getCartesian2Normal(position);
        break;
        case 2:
        pos4 = vec4(getSpherical1(position), 1.0);
        normal = mat3(view) * getSpherical1Normal(position);
        break;
        case 3:
        pos4 = vec4(getSpherical2(position), 1.0);
        normal = mat3(view) * getSpherical2Normal(position);
        break;
        case 4:
        pos4 = vec4(getCylindrical1(position), 1.0);
        normal = mat3(view) * getCylindrical1Normal(position);
        break;
        case 5:
        pos4 = vec4(getCylindrical2(position), 1.0);
        normal = mat3(view) * getCylindrical2Normal(position);
        break;
        case 6:
        pos4 = vec4(getLightSphere(position), 1.0);
        normal = mat3(view) * getLightSphereNormal(position);
        break;
        case 7:
        pos4 = vec4(getPlane(position), 1.0);
        normal = mat3(view) * getPlaneNormal(position);
        break;
    }

	gl_Position = projection * view * pos4;

    texCoord = inPosition;

    vertColorXYZ = pos4.xyz;
    vertColorXYZ.x = vertColorXYZ.x / 9 + 1;
    vertColorXYZ.y = vertColorXYZ.y / 6 + 1;
    vertColorXYZ.z = vertColorXYZ.z + 2;

    vec3 lightPos = vec3(1,1,0);
    light = lightPos - (view * pos4).xyz;

    viewDirection = - (view * pos4).xyz;

    depthTextureCoord = lightViewProjection * pos4;
    depthTextureCoord.xyz = depthTextureCoord.xyz / depthTextureCoord.w;
    depthTextureCoord.xyz = ( depthTextureCoord.xyz + 1 ) / 2;
}
