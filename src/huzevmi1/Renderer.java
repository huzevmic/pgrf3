package huzevmi1;


import lwjglutils.*;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import transforms.*;

import java.io.IOException;
import java.nio.DoubleBuffer;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_F;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;


/**
 *
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2019-09-02
 */
public class Renderer extends AbstractRenderer{

    double ox, oy;
    private boolean mouseButton1 = false;

    private int shaderProgramViewer, shaderProgramLight;
    private OGLBuffers buffers;
    private int locView, locProjection, locTime, locType, locLightVP, locColorMode, locCamShift;

    private Camera camera, cameraLight;
    private Mat4 projection;

    private OGLRenderTarget renderTarget;
    private OGLTexture2D.Viewer viewer;
    private OGLTexture2D texture;
    private int locViewLight, locProjectionLight, locTimeLight, locTypeLight;

    private float time = 0;
    private boolean line = false;
    private boolean ortho = false;
    private float timediff = 0.05f;
    private final float delta = 1e-4f;
    private int colorMode = 0;
    private float camShift = 6.5f;
    private float camShiftVal = -0.02f;
    private int camShiftDirection = -1;

    @Override
    public void init() {
        glClearColor(0.1f,0.1f,0.1f,1);
        glEnable(GL_DEPTH_TEST);

        shaderProgramViewer = ShaderUtils.loadProgram("/start");
        shaderProgramLight = ShaderUtils.loadProgram("/light");

        locView = glGetUniformLocation(shaderProgramViewer, "view");
        locProjection = glGetUniformLocation(shaderProgramViewer, "projection");
        locTime = glGetUniformLocation(shaderProgramViewer, "time");
        locType = glGetUniformLocation(shaderProgramViewer, "type");
        locLightVP = glGetUniformLocation(shaderProgramViewer, "lightViewProjection");
        locColorMode = glGetUniformLocation(shaderProgramViewer, "colorMode");
        locCamShift = glGetUniformLocation(shaderProgramViewer, "camShift");

        locViewLight = glGetUniformLocation(shaderProgramLight, "view");
        locProjectionLight = glGetUniformLocation(shaderProgramLight, "projection");
        locTimeLight = glGetUniformLocation(shaderProgramLight, "time");
        locTypeLight = glGetUniformLocation(shaderProgramLight, "type");

        try {
            texture = new OGLTexture2D("./textures/bricks.jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }

        buffers = GridFactory.generateGrid(100,100);

        renderTarget = new OGLRenderTarget(1024, 1024);
        viewer = new OGLTexture2D.Viewer();

        cameraLight = new Camera()
                .withPosition(new Vec3D(6,3,camShift))
                .withAzimuth(5/4f * Math.PI)
                .withZenith(-1/5f * Math.PI);

        camera = new Camera()
                .withPosition(new Vec3D(1,3,3))
                .withAzimuth(6/4f * Math.PI)
                .withZenith(-1/4f * Math.PI)
                .withFirstPerson(false)
                .withRadius(6);

        projection = new Mat4PerspRH(Math.PI / 3, LwjglWindow.HEIGHT/(float)LwjglWindow.WIDTH, 1, 20);
    }

    @Override
    public void display() {
        time += timediff;
        camShift += camShiftVal;
        if ( camShift < 4 ) {
            camShiftVal = 0.02f;
            camShiftDirection = 1;
        }
        if ( camShift > 6.5f ) {
            camShiftVal = -0.02f;
            camShiftDirection = -1;
        }
        cameraLight = new Camera()
                .withPosition(new Vec3D(6, 3, camShift))
                .withAzimuth(5/4f * Math.PI)
                .withZenith(-1/5f * Math.PI);

        renderFromLight();
        renderFromViewer();

        viewer.view(renderTarget.getColorTexture(), -1, 0, 0.5);
    }

    private void renderFromLight() {
        glUseProgram(shaderProgramLight);
        renderTarget.bind();

        glClearColor(0, 0.5f, 0, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUniformMatrix4fv(locViewLight, false, cameraLight.getViewMatrix().floatArray());
        glUniformMatrix4fv(locProjectionLight, false, projection.floatArray());
        glUniform1f(locTimeLight, time);

        for (int i = 0; i < 8; i++) {
            glUniform1i(locTypeLight, i);
            buffers.draw(GL_TRIANGLES, shaderProgramLight);
        }
    }

    private void renderFromViewer() {
        glUseProgram(shaderProgramViewer);
        glViewport(0,0,width,height);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        glClearColor(0.5f, 0, 0, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUniformMatrix4fv(locView, false, camera.getViewMatrix().floatArray());
        glUniformMatrix4fv(locProjection, false, projection.floatArray());
        glUniformMatrix4fv(locLightVP, false, cameraLight.getViewMatrix().mul(projection).floatArray());
        glUniform1f(locTime, time);
        glUniform1i(locColorMode, colorMode);
        glUniform1f(locCamShift, camShift);

        renderTarget.getColorTexture().bind(shaderProgramViewer, "depthTexture", 0);

//        glUniform3fv(, ToFloatArray.convert(cameraLight.getPosition()));

        texture.bind(shaderProgramViewer, "textureBricks", 1);

        for (int i = 0; i < 8; i++) {
            glUniform1i(locType, i);
            buffers.draw(GL_TRIANGLES, shaderProgramViewer);
        }
    }

    @Override
    public GLFWKeyCallback getKeyCallback() {
        return new GLFWKeyCallback() {
            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
                if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
                    glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
                if (action == GLFW_PRESS || action == GLFW_REPEAT) {
                    switch (key) {
                        case GLFW_KEY_W:
                            camera = camera.forward(1);
                            break;
                        case GLFW_KEY_D:
                            camera = camera.right(1);
                            break;
                        case GLFW_KEY_S:
                            camera = camera.backward(1);
                            break;
                        case GLFW_KEY_A:
                            camera = camera.left(1);
                            break;
                        case GLFW_KEY_C:
                            camera = camera.down(1);
                            break;
                        case GLFW_KEY_SPACE:
                            camera = camera.up(1);
                            break;
                        case GLFW_KEY_LEFT_SHIFT:
                            camera = camera.withFirstPerson(!camera.getFirstPerson());
                            break;
                        case GLFW_KEY_R:
                            camera = camera.mulRadius(0.9f);
                            break;
                        case GLFW_KEY_F:
                            camera = camera.mulRadius(1.1f);
                            break;
                        case GLFW_KEY_U:
                            glPolygonMode(GL_FRONT_AND_BACK, line ? GL_FILL : GL_LINE);
                            line = !line;
                            break;
                        case GLFW_KEY_I:
                            if ( timediff < delta ) {
                                timediff = 0.05f;
                                if ( camShiftDirection == 1 ) {
                                    camShiftVal = 0.02f;
                                } else {
                                    camShiftVal = -0.02f;
                                }
                            } else {
                                timediff = 0f;
                                camShiftVal = 0f;
                            }
                            break;
                        case GLFW_KEY_O:
                            if ( ortho ) {
                                projection = new Mat4PerspRH(Math.PI / 3, LwjglWindow.HEIGHT/(float)LwjglWindow.WIDTH, 1, 20);
                            } else {
                                projection = new Mat4OrthoRH(LwjglWindow.WIDTH /50f, LwjglWindow.HEIGHT /50f, 1, 20);
                            }
                            ortho = !ortho;
                            break;
                        case GLFW_KEY_P:
                            colorMode = (colorMode + 1) % 5;
                            break;
                        case GLFW_KEY_J:
                            break;
                        case GLFW_KEY_K:
                            break;
                        case GLFW_KEY_L:
                            break;
                    }
                }
            }
        };
    }

    @Override
    public GLFWCursorPosCallback getCursorCallback() {
        return new GLFWCursorPosCallback() {
            @Override
            public void invoke(long window, double x, double y) {
                if (mouseButton1) {
                    camera = camera.addAzimuth((double) Math.PI * (ox - x) / width)
                            .addZenith((double) Math.PI * (oy - y) / width);
                    ox = x;
                    oy = y;
                }
            }
        };
    }

    @Override
    public GLFWMouseButtonCallback getMouseCallback() {
        return new GLFWMouseButtonCallback () {

            @Override
            public void invoke(long window, int button, int action, int mods) {
                mouseButton1 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS;

                if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS){
                    mouseButton1 = true;
                    DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                    DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                    glfwGetCursorPos(window, xBuffer, yBuffer);
                    ox = xBuffer.get(0);
                    oy = yBuffer.get(0);
                }

                if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE){
                    mouseButton1 = false;
                    DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                    DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                    glfwGetCursorPos(window, xBuffer, yBuffer);
                    double x = xBuffer.get(0);
                    double y = yBuffer.get(0);
                    camera = camera.addAzimuth((double) Math.PI * (ox - x) / width)
                            .addZenith((double) Math.PI * (oy - y) / width);
                    ox = x;
                    oy = y;
                }
            }

        };
    }
}